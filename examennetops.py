import requests
import json
import re
import yaml

json = requests.get('https://api.domainsdb.info/v1/domains/search?domain=syntra.be').json()
domains = json["domains"]
createddate = domains[0]["create_date"]
nameserver = domains[0]["NS"][0]
ip = domains[0]["A"][0]
jaar = re.match("([0-9]+)-([0-9]+)-([0-9]+)",createddate).group(1)
maand = re.match("([0-9]+)-([0-9]+)-([0-9]+)",createddate).group(2)
dag = re.match("([0-9]+)-([0-9]+)-([0-9]+)",createddate).group(3)
provider = re.search("\.([^\.]+)\.",nameserver).group(1)
land = domains[0]["country"]
config = [{'created': {'dag':dag,'jaar':jaar,'maand':maand}},
         {'ip': ip},{'land': land},{'provider':provider}]
print(yaml.dump(config))